#!/usr/bin/env bash

systemctl enable ssh-agent --user --now
systemctl enable gpg-agent.socket gpg-agent-extra.socket gpg-agent-browser.socket --user --now
