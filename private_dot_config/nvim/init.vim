" #######
" Plugins configuration
" #######
call plug#begin('~/.vim/plugged')

Plug 'arcticicestudio/nord-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'preservim/nerdtree'
Plug 'neovim/nvim-lspconfig'
Plug 'preservim/nerdtree'

call plug#end()

" #######
" Appearance configuratio
" #######
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
colorscheme nord

" ######
" NerdTreeKeymap
" ######
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>


" ######
" LSP
" ######
lua << LUA
require'lspconfig'.powershell_es.setup{}
require'lspconfig'.yamlls.setup{}
require'lspconfig'.kotlin_language_server.setup{}
LUA
