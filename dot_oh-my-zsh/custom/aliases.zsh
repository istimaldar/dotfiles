alias ll="exa -@laFhmuiHg --color-scale --git --group-directories-first"
alias wusaup="sudo wg-quick up wg-usa"
alias wusadn="sudo wg-quick down wg-usa"
alias wfinup="sudo wg-quick up wg-finland"
alias wfindn="sudo wg-quick down wg-finland"
alias dcc="docker-compose -p coin_server --file ${HOME}/code/coin_server/build/docker-compose.yaml --file ${HOME}/code/coin_server/build/docker-compose.dev.yaml --file ${HOME}/code/coin_server/build/docker-compose.debug.yaml"
alias mdud="make -C ${HOME}/code/coin_server docker-up-dev"
alias mddd="make -C ${HOME}/code/coin_server docker-down-dev"
alias dsa="sudo systemctl start docker"
alias dso="sudo systemctl stop docker"

alin() {
  aws ec2 describe-instances ${1} | jq -r '[.Reservations | map(.Instances)[][] | select(.Tags) | {Name: (.Tags[] | select(.Key=="Name").Value), Ip: .PublicIpAddress}]' 
}
