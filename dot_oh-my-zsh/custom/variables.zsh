export EDITOR="nvim"
export PATH="/opt/chef-workstation/embedded/bin:${PATH}:${HOME}/.dotnet/tools:${HOME}/go/bin:${HOME}/.poetry/bin:${HOME}/.yarn/bin"
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk"
