#!/usr/bin/env bash

ZSH_HOME=$HOME/.oh-my-zsh;


if [[ ! -d $ZSH_HOME ]];
then
    mkdir -p $HOME/.oh-my-zsh;
fi

if [[ ! -L $ZSH_HOME/oh-my-zsh.sh && -f /usr/share/oh-my-zsh/oh-my-zsh.sh ]];
then
    ln -s /usr/share/oh-my-zsh/oh-my-zsh.sh $ZSH_HOME/oh-my-zsh.sh;
fi

for ZSH_MODULE in lib plugins templates themes tools;
do
    if [[ ! -L $ZSH_HOME/$ZSH_MODULE && -d /usr/share/oh-my-zsh/$ZSH_MODULE ]];
    then
        ln -s /usr/share/oh-my-zsh/$ZSH_MODULE $ZSH_HOME/$ZSH_MODULE
    fi
done

ZSH_PLUGINGS_DIR=$ZSH_HOME/custom/plugins;

if [[ ! -d $ZSH_PLUGINGS_DIR ]];
then
    mkdir -p $ZSH_PLUGINGS_DIR;
fi

for ZSH_PLUGIN in zsh-autosuggestions zsh-syntax-highlighting;
do
    if [[ ! -L $ZSH_PLUGINGS_DIR/$ZSH_PLUGIN && -d /usr/share/zsh/plugins/$ZSH_PLUGIN ]];
    then
        ln -s /usr/share/zsh/plugins/$ZSH_PLUGIN $ZSH_PLUGINGS_DIR/$ZSH_PLUGIN;
    fi
done

ZSH_THEMES_DIR=$ZSH_HOME/custom/themes;

if [[ ! -d $ZSH_THEMES_DIR ]];
then
    mkdir -p $ZSH_THEMES_DIR;
fi

for ZSH_THEME in powerlevel10k;
do
    if [[ ! -L $ZSH_THEMES_DIR/$ZSH_THEME && -d /usr/share/zsh-theme-$ZSH_THEME ]];
    then
        ln -s /usr/share/zsh-theme-$ZSH_THEME $ZSH_THEMES_DIR/$ZSH_THEME;
    fi
done